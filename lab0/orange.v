
module orange(
    input wire A,
    input wire B,
    input wire clk,
    output reg Y
);

always@(posedge clk) begin

	Y <= A | B;

end

endmodule
