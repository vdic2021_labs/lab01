

module orange_tb();

	wire A;
	wire B;
	wire clk;
	wire Y;

	orange u_orange (
		.A  (A),
		.B  (B),
		.Y  (Y),
		.clk(clk)
	);

endmodule


